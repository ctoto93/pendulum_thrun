from planner import Planner
import numpy as np

class Agent:

    def __init__(self, env, planner):
        self.env = env
        self.planner = planner

    def run(self, angle=None):
        done = False
        if angle is not None:
            self.env.set_state([np.radians(angle), 0])

        state = self.env.get_state()
        while not done:
            self.env.render()
            action = self.planner.get_action(state)
            state, _, done, _ = self.env.step(action)

    def test_angle(self, angle, speed, steps):
        self.env.reset()
        rad = np.radians(angle)
        self.env.set_state([rad, speed])
        state = self.env.get_state()
        rad_hist = [rad]
        for step in range(steps):
            print(f"--- processing step {step} ---")
            print(state.shape)
            action = self.planner.get_action(state)
            state, _, _, _ = self.env.step(action)
            rad_hist.append(self.env.env.state[0])

        return np.degrees(rad_hist)

    def reset(self):
        self.env.reset()
        self.planner.reset()
