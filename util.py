DEFAULT_MODELS_PATH = "models/"

def file_full_path(model_name, filename):
    return "{}/{}/{}".format(DEFAULT_MODELS_PATH, model_name, filename)

def model_path(model_name):
    return "{}/{}".format(DEFAULT_MODELS_PATH, model_name)
