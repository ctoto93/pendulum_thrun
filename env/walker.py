import gym
from env.core import EnvWrapper

class WalkerWrapper(EnvWrapper):
    def __init__(self, state=None):
        self.env = gym.make("RoboschoolWalker2d-v1")
        if state is not None:
            theta, thetadot = state
            self.env.env.state = np.array([theta, thetadot])

# TODO: implement get and set state
