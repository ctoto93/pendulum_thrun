class EnvWrapper:

    def get_state(self):
        raise NotImplementedError

    def set_state(self, state):
        raise NotImplementedError

    def step(self, action):
        return self.env.step([action])

    def reset(self):
        return self.env.reset()

    def close(self):
        self.env.close()

    def render(self, mode='human'):
        self.env.render(mode)

    @property
    def observation_space(self):
        return self.env.observation_space

    @property
    def action_space(self):
        return self.env.action_space
