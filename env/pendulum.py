import gym
from env.core import EnvWrapper

class PendulumWrapper(EnvWrapper):
    def __init__(self, state=None):
        self.env = gym.make("Pendulum-v0").unwrapped
        self.env.reset()
        if state is not None:
            theta, thetadot = state
            self.env.state = np.array([theta, thetadot])

    def get_state(self):
        return self.env._get_obs()

    def set_state(self, state):
        self.env.state = state
