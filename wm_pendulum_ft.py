from __future__ import print_function
import numpy as np
import tensorflow.keras as keras
import sys
import pickle

from hyperopt import Trials, STATUS_OK, tpe
from keras.layers.core import Dense, Dropout, Activation
from keras.models import Sequential
from keras.utils import np_utils
from sklearn.model_selection import train_test_split

from hyperas import optim
from hyperas.distributions import choice, uniform

from util import file_full_path
from command.new_world_model import plot_history

def data():
    from world_model import WorldModel
    from env import PendulumWrapper
    from util import file_full_path
    import tensorflow as tf
    import time

    env = PendulumWrapper()

    features = [] # current state + action
    targets = [] # next state
    for _ in range(1000):
        state = env.reset()
        action = env.action_space.sample()
        features.append(WorldModel.input(state, action))
        next_state, _, _, _ = env.step(action)
        targets.append(next_state)

    features = np.array(features)
    targets = np.array(targets)
    targets = np.squeeze(targets, axis=2)

    X_train, X_test, y_train, y_test = train_test_split(
            features,
            targets,
            test_size=0.2)

    scaler = StandardScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    temp = np.zeros(X_train.shape)
    temp[:,:-1] = y_train

    y_train = scaler.transform(temp)[:,:-1]

    temp = np.zeros(X_test.shape)
    temp[:,:-1] = y_test

    y_test = scaler.transform(temp)[:,:-1]

    scaler_path = "models/pendulum_std_2/world_model_scaler.pkl"
    with open(scaler_path, 'wb') as fp:
        pickle.dump(scaler, fp)

    return X_train, y_train, X_test, y_test

def create_model(X_train, y_train, X_test, y_test):
    env = PendulumWrapper()
    model = Sequential()

    model.add(Dense({{choice([8, 16, 32, 64, 128])}}, input_shape=(4,)))
    model.add(Activation({{choice(['tanh'])}}))
    model.add(Dropout({{uniform(0, 1)}}))

    num_layers = ['one', 'two']
    if num_layers == 'two':
        model.add(Dense({{choice([8, 16, 32, 64, 128])}}))
        model.add(Activation({{choice(['tanh'])}}))
        model.add(Dropout({{uniform(0, 1)}}))

    model.add(Dense(3))

    model.compile(loss='mse', metrics=['mae'],
              optimizer={{choice(['rmsprop', 'adam', 'sgd'])}})

    result = model.fit(X_train, y_train,
              batch_size={{choice([16])}},
              epochs=25,
              verbose=0,
              validation_data=(X_test, y_test))
    #get the highest validation accuracy of the training epochs
    val_loss = np.amax(result.history['val_loss'])
    print('Best validation loss of epoch:', val_loss)

    return {'loss': val_loss, 'status': STATUS_OK, 'model':model, 'history': result.history}

if __name__ == '__main__':
    DENSE_UNIT_CHOICES = [8, 16, 32, 64, 128]
    ACTIVATION_CHOICES = ['tanh']
    OPTIMIZER_CHOICES = ['rmsprop', 'adam', 'sgd']
    BATCH_SIZE_CHOICES = [16]
    MODEL_NAME = "pendulum_std_2"
    sys.stdout = open('models/{}/log.txt'.format(MODEL_NAME), 'w+')

    trials = Trials()
    best_run, best_model = optim.minimize(model=create_model,
                                          data=data,
                                          algo=tpe.suggest,
                                          max_evals=200,
                                          trials=trials)
    X_train, Y_train, X_test, Y_test = data()
    print("Evalutation of best performing model:")
    print(best_model.evaluate(X_test, Y_test))

    print("Best performing model chosen hyper-parameters:")
    print(f"Dense: {DENSE_UNIT_CHOICES[best_run['Dense']]} ")
    print(f"Activation: {ACTIVATION_CHOICES[best_run['Activation']]} ")
    print(f"Dropout: {best_run['Dropout']} ")

    print(f"Dense_1: {DENSE_UNIT_CHOICES[best_run['Dense_1']]} ")
    print(f"Activation_1: {ACTIVATION_CHOICES[best_run['Activation_1']]} ")
    print(f"Dropout_1: {best_run['Dropout_1']} ")

    print(f"optimizer: {OPTIMIZER_CHOICES[best_run['optimizer']]} ")
    print(f"batch_size: {BATCH_SIZE_CHOICES[best_run['batch_size']]} ")

    best_model_result = list(filter(lambda result: result['model'] == best_model, trials.results))[0]
    print(f"last_val_mae: {best_model_result['history']['val_mae'][-1]} ")
    plot_history(best_model_result['history'], MODEL_NAME)


    model_path = file_full_path(MODEL_NAME, 'world_model.h5')
    best_model.save(model_path)
    with open(file_full_path(MODEL_NAME, 'history.pkl'), 'wb') as fp:
        pickle.dump(best_model_result['history'], fp)
