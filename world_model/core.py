from tensorflow.keras import models
import numpy as np
import pickle
import tensorflow as tf
from util import file_full_path

MODEL_FILENAME = "world_model.h5"
SCALER_FILENAME = "world_model_scaler.pkl"

def load_world_model(cls, env, model_name):
    model_path = file_full_path(model_name, MODEL_FILENAME)
    model, scaler = load_model_and_scaler(model_name)
    return cls(env, model, scaler)

def load_model_and_scaler(model_name):
    model_path = file_full_path(model_name, MODEL_FILENAME)
    scaler_path = file_full_path(model_name, SCALER_FILENAME)

    model = models.load_model(model_path, compile=False)
    with open(scaler_path, 'rb') as f:
        scaler = pickle.load(f)

    return model, scaler

class WorldModel:
    def input(state, action):
        return np.concatenate((state, action), axis=None)

    def __init__(self, env, model, scaler):
        self.env = env
        self.model = model
        self.scaler = scaler

    def reward(self, state, action):
        raise Exception("reward not yet implemented")
        pass

    def save(self, model_name):
        model_path = file_full_path(model_name, MODEL_FILENAME)
        scaler_path = file_full_path(model_name, SCALER_FILENAME)
        self.model.save(model_path)
        with open(scaler_path, 'wb') as fp:
            pickle.dump(self.scaler, fp)

    def to_neural_input(self, state, action):
        input = tf.concat([state, action], axis=1)

        input -= tf.convert_to_tensor(self.scaler.mean_, dtype="float32")
        input /= tf.convert_to_tensor(self.scaler.scale_, dtype="float32")

        return input

    def to_actual_state(self, neuron_state):
        temp = neuron_state
        temp *= tf.convert_to_tensor(self.scaler.scale_[:-1], dtype="float32")
        temp += tf.convert_to_tensor(self.scaler.mean_[:-1], dtype="float32")
        return temp
