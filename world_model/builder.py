from world_model import WorldModel
from tensorflow.keras import models
from tensorflow.keras.layers import Dense
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler

import numpy as np

_prop_defaults = {
    "model": None,
    "optimizer": "adam",
    "loss": "mse",
    "metrics": ["mae"],
    "n_samples": 4000,
    "epochs": 100,
    "batch_size": 1,
    "validation_split": 0.2,
    "scaler": StandardScaler()
}

class WorldModelBuilder:
    def __init__(self, env, hidden_layers, **kwargs):
        for (prop, default) in _prop_defaults.items():
            setattr(self, prop, kwargs.get(prop, default))

        self.env = env
        self.hidden_layers = hidden_layers
        self.n_inputs = env.observation_space.shape[0] + env.action_space.shape[0]
        self.n_outputs = env.observation_space.shape[0]
        if not self.model:
            self.model = self.build_keras_model()

    def build_keras_model(self):
        model = models.Sequential()
        model.add(Dense(self.hidden_layers[0], activation='relu', input_shape=(self.n_inputs,)))
        for neurons in self.hidden_layers[1:]:
            model.add(Dense(neurons, activation='tanh'))

        model.add(Dense(self.n_outputs))
        model.compile(optimizer=self.optimizer,
                      loss=self.loss,
                      metrics=self.metrics)
        return model

    def generate_samples(self, n):
        features = [] # current state + action
        targets = [] # next state
        for _ in range(n):
            state = self.env.reset()
            action = self.env.action_space.sample()
            features.append(WorldModel.input(state, action))
            next_state, _, _, _ = self.env.step(action)
            targets.append(next_state)

        features = np.array(features)
        targets = np.array(targets)
        targets /= self.env.observation_space.high
        # print(f"unique: {len(np.unique(features))}")
        return train_test_split(
                features,
                targets,
                test_size=self.validation_split)

    def train(self):
        X_train, X_test, y_train, y_test = self.generate_samples(self.n_samples)
        X_train = self.scaler.fit_transform(X_train)
        X_test = self.scaler.transform(X_test)

        history = self.model.fit(X_train, y_train,
                                batch_size=self.batch_size,
                                epochs=self.epochs,
                                validation_data=(X_test, y_test))

        return WorldModel(self.env, self.model, self.scaler), history
