import tensorflow as tf
import numpy as np
from world_model import WorldModel

@tf.function
def reward_theta(state, action):
    th, _ = self.env.env.state
    th = tf.convert_to_tensor([th], dtype=tf.float32)

    rewards = -(tf.square(th)
            + 0.1 * tf.square(state[:, 2])
            + 0.001 * tf.square(action)
            )
    return rewards

@tf.function
def reward_abs_sin(state, action):
    state = tf.convert_to_tensor(state)
    action = tf.convert_to_tensor(action)

    rewards = -(tf.square(tf.math.atan2(state[:, 1], state[:, 0]))
            + 0.1 * tf.square(state[:, 2])
            + 0.001 * tf.square(action[:, 0])
            )
    return rewards

@tf.function
def reward_cos(state, action):
    rewards = -(tf.square(state[:, 0] - 1)
            + 0.1 * tf.square(state[:, 2])
            + 0.001 * tf.square(action)
            )
    return rewards

def angle_normalize(x):
    return (((x+np.pi) % (2*np.pi)) - np.pi)

class PendulumWorldModel(WorldModel):
    def __init__(self, env, model, scaler, reward_func=reward_abs_sin):
        super().__init__(env, model, scaler)
        self.reward = reward_func
