import sys
import pathlib
from env import PendulumWrapper, WalkerWrapper

from command import new_world_model, run_agent, test_steps, test_angle, \
    test_world_model
from util import model_path
from world_model import WalkerWorldModel, PendulumWorldModel

command = sys.argv[1]
env_name = sys.argv[2]
index = sys.argv[3]

model_name = "{}_{}".format(env_name, index)

if env_name == "walker":
    env = WalkerWrapper()
    cls = WalkerWorldModel
    config = {
        "hidden_layers": [64,64,32],
        "batch_size": 20,
        "epochs": 200,
        "n_samples": 50000
    }
elif env_name == "pendulum":
    env = PendulumWrapper()
    cls = PendulumWorldModel
    config = {
        "hidden_layers": [16,16],
        "batch_size": 20,
        "epochs": 400,
        "n_samples": 1000
    }
else:
    raise Exception("invalid env name. (walker/pendulum)")

if command == "new":
    path = model_path(model_name)
    pathlib.Path(path).mkdir(parents=True, exist_ok=True)
    new_world_model(env, model_name, config)
elif command == "predict":
    test_predict(cls, env, model_name)
elif command == 'run_agent':
    angle = float(sys.argv[4])
    run_agent(cls, env, model_name, angle)
elif command == 'test_steps':
    test_steps(cls, env, model_name)
elif command == 'test_angle':
    test_angle(env, model_name)
elif command == 'test_world_model':
    test_world_model(env, model_name)
elif command == 'generate_samples':
    generate_data(env, 1000)
else:
    raise Exception("invalid command. (new/predict)")
