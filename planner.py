import collections
import numpy as np
import tensorflow as tf
from world_model import WorldModel
import pyswarms as ps

options = {'c1': 1.49445, 'c2': 1.49445, 'w':0.729}

class Planner:

    def __init__(self, env, world_model, iterations=3000, length=25, learning_rate=0.1, particles=15, discounted_factor=0.9):
        self.env = env
        self.world_model = world_model
        self.iterations = iterations
        self.length = length
        self.particles = particles
        self.discounted_factor = discounted_factor
        self.history = []
        self.reset()

    def new_optimizer(self):
        x_max = 2 * np.ones(self.length)
        x_min = -1 * x_max
        bounds = (x_min, x_max)

        optimizer = ps.single.GlobalBestPSO(
                n_particles=self.particles,
                dimensions=self.length,
                options=options,
                bounds=bounds)

        return optimizer

    def get_action(self, state, grad_log_callback=None):
        self.state = tf.convert_to_tensor([state], dtype="float32")
        self.plan = self.generate_plan()
        return self.plan.popleft()

    def generate_plan(self):
        optimizer = self.new_optimizer()
        best_cost, best_pos = optimizer.optimize(self.e_reinf, iters=self.iterations)
        self.history.append({
            "state": self.env.env.state,
            "cost_history": optimizer.cost_history,
            "plan": best_pos
        })
        return collections.deque(best_pos)

    def e_reinf(self, x):
        n_particles, n_plan = x.shape
        actual_state = tf.keras.backend.repeat_elements(self.state, rep=n_particles, axis=0)
        cum_losses = tf.fill([n_particles], 0.0)
        for n in range(n_plan):
            action = tf.convert_to_tensor(x[:,n], dtype="float32")
            action = tf.reshape(action, [n_particles, 1])
            input = self.world_model.to_neural_input(actual_state, action)
            output = self.world_model.model(input)
            actual_state = self.world_model.to_actual_state(output)
            loss = self.world_model.reward(actual_state, [action]) * -1
            cum_losses += tf.math.pow(tf.fill([n_particles], self.discounted_factor), n) * loss

        return tf.squeeze(cum_losses)

    def random_action(self):
        return tf.Variable( 0.0 , dtype=tf.float32)

    def reset(self):
        plan = []
        self.plan = collections.deque(plan)
