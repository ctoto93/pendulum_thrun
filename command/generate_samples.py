import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from world_model import WorldModel

def generate_samples(env, n, validation_split=0.2):
    features = [] # current state + action
    targets = [] # next state
    for _ in range(n):
        state = env.reset()
        action = env.action_space.sample()
        features.append(WorldModel.input(state, action))
        next_state, _, _, _ = env.step(action)
        targets.append(next_state)

    features = np.array(features)
    targets = np.array(targets)

    return train_test_split(
            features,
            targets,
            test_size=validation_split)
