from agent import Agent
from world_model import load_world_model
from planner import Planner
from util import file_full_path
import matplotlib.pyplot as plt

def get_default_agent(cls, env, model_name):
    world_model = load_world_model(cls, env, model_name)
    planner = Planner(env, world_model)
    agent = Agent(env, planner)

    return agent, planner, world_model

def run_agent(cls, env, model_name, angle):
    agent, _, _ = get_default_agent(cls, env, model_name)
    agent.run(angle)
