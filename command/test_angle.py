import pandas as pd
import matplotlib.pyplot as plt
from agent import Agent
from env import PendulumWrapper
from world_model import PendulumWorldModel, reward_abs_sin, reward_cos, load_model_and_scaler
from planner import Planner

def test_angle(env, model_name):
    for reward_func in [reward_abs_sin]:
        print(f"xxx {reward_func.__name__} xxx")
        for discounted_factor in [0.9, 1.1]:
            print(f"### DISCOUNTED FACTOR {discounted_factor} ###")
            env = PendulumWrapper()
            model, scaler = load_model_and_scaler(model_name)
            wm = PendulumWorldModel(env, model, scaler, reward_func)
            planner = Planner(env, wm, iterations=50, particles=1000, discounted_factor=discounted_factor)
            agent = Agent(env, planner)

            steps = 100
            index = list(range(steps+1))

            plt.figure(figsize=(20,10))

            for angle in [-30, -15, -10, -5, 0, 5, 10, 15, 30]:
                print(f"*********** ANGLE {angle} ***********")
                angle_hist = agent.test_angle(angle, 0, steps)
                plt.plot(index, angle_hist, label=str(angle))
                plt.legend()

            plt.savefig("figs/angle_test_{}_{}_dis{}.png".format(model_name, reward_func.__name__, discounted_factor))
            plt.clf()
