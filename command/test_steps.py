import numpy as np
import tensorflow as tf
from agent import Agent
from world_model import load_world_model

def test_steps(cls, env, model_name):
    state = env.reset()
    state = tf.convert_to_tensor([state], dtype=tf.float32)
    world_model = load_world_model(cls, env, model_name)
    while True:
        action = env.action_space.sample()
        reward = world_model.reward(state, [action])
        state = world_model.predict_next_state(state, action)
        print(f"next state: {state} \n reward: {reward}")
        if reward[0][0] == -np.inf:
            break
