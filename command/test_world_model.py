import pandas as pd
import seaborn as sns
from sklearn.metrics import mean_absolute_error
from world_model import load_world_model, PendulumWorldModel
import tensorflow as tf

def test_world_model(env, model_name, samples=50, steps=50):
    wm = load_world_model(PendulumWorldModel, env, model_name)
    data = []
    for n in range(samples):
        predicted_states = []
        actual_states = []

        state = wm.env.reset()
        state = tf.convert_to_tensor([state], dtype="float32")

        # both predicted and actual start from same state initially
        actual_state = state
        predicted_state = state

        for step in range(steps):
            action = wm.env.action_space.sample()
            action = tf.convert_to_tensor(action, dtype="float32")

            input = wm.to_neural_input(predicted_state, action)
            output = wm.model.predict(input)
            predicted_state = wm.to_actual_state(output).numpy()
            actual_state, _, _, _ = wm.env.step(action)

            predicted_states.append(predicted_state.flatten())
            actual_states.append(actual_state)

        step = 0
        for actual, predicted in zip(actual_states, predicted_states):
            mae = mean_absolute_error(actual, predicted)
            data.append([n, step, mae])
            step += 1

    df = pd.DataFrame(data, columns=["n", "step", "mae"])
    plot_model_analysis(model_name, df)

def plot_model_analysis(model_name, df):
    palette = sns.cubehelix_palette(
        n_colors=len(df["n"].unique()),
        start=1,
        rot=-.8,
        hue=1,
        dark=0.4,
        light=0.75
    )

    df_mean = df.groupby("step").mean()
    df_mean = df_mean.drop(columns="n")
    df_mean.reset_index(inplace=True)

    fig = sns.relplot(x="step",
                        y="mae",
                        hue="n",
                        palette=palette,
                        alpha=0.5,
                        height=5,
                        aspect=6/2,
                        kind="line",
                        legend=False,
                        data=df
                        )
    sns.lineplot(x="step",
                     y="mae",
                     linewidth=4,
                     color="cyan",
                     data=df_mean,
                     ax=fig.ax
                     )
    fig.savefig("figs/model_analysis_{}".format(model_name))
