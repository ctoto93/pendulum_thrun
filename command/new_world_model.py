import roboschool
import gym
import pickle
from world_model import WorldModelBuilder
from copy import copy
from util import file_full_path
import matplotlib.pyplot as plt


PLOT_MSE_FILENAME = "plot_mse.png"
PLOT_MAE_FILENAME = "plot_mae.png"
HISTORY_FILENAME = "history.pkl"

def plot_history(history, model_name):
    loss = history['loss']
    val_loss = history['val_loss']
    mae = history['mae']
    val_mae = history['val_mae']
    epochs = range(1, len(loss) + 1)

    plot(loss, val_loss, "MSE", file_full_path(model_name, PLOT_MSE_FILENAME))
    plot(mae, val_mae, "MAE", file_full_path(model_name, PLOT_MAE_FILENAME))

def plot(train, val, metric, fig_path=None):
    epochs = range(1, len(train) + 1)
    plt.plot(epochs, train, label="train_{}".format(metric))
    plt.plot(epochs, val, label="val_{}".format(metric))
    plt.xlabel('Epochs')
    plt.ylabel(metric)
    plt.legend()

    if fig_path:
        plt.savefig(fig_path)
    else:
        plt.show()

    plt.clf()


def new_world_model(env, model_name, config):
    state = env.reset()
    world_model_builder = WorldModelBuilder(
                                env=copy(env),
                                n_samples=config["n_samples"],
                                hidden_layers=config["hidden_layers"],
                                batch_size=config["batch_size"],
                                epochs=config["epochs"]
                            )
    world_model_builder.model.summary()
    world_model, history = world_model_builder.train()

    world_model.save(model_name)
    with open(file_full_path(model_name, HISTORY_FILENAME), 'wb') as fp:
        pickle.dump(history.history, fp)

    plot_history(history.history, model_name)
